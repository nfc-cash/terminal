from mfrc522 import SimpleMFRC522
import RPi.GPIO as GPIO
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.Certificate("firebase_admin_key.json")
firebase_admin.initialize_app(cred)
db = firestore.client()

reader = SimpleMFRC522()

def read():
    try:
        while True:
            id = reader.read_id()
            return id
    except:
        print("Error while reading")
    finally:
        GPIO.cleanup()

print("Ready")

while True:
    uid = read()
    print(uid)
    db.collection(u'nfcChips').document(str(uid)).set({})
