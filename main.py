from RPi_GPIO_i2c_LCD import lcd
from time import sleep
import sys
from mfrc522 import SimpleMFRC522
import RPi.GPIO as GPIO
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import urllib.request

# initialize lcd first to notify user about ongoing initialization
lcd = lcd.HD44780(0x27)

lcd.set("Initialisierung",1)
lcd.set("...",2)

# initialize firebase
cred = credentials.Certificate("firebase_admin_key.json")
firebase_admin.initialize_app(cred)
db = firestore.client()

# cleanup bridge collection
db.collection(u'bridge').document('process').delete()

# initialize reader
reader = SimpleMFRC522()

## start define functions 

# Handle doc changes
def handler(col_snapshot, changes, read_time):
    for change in changes:
        if change.type.name == 'ADDED' or change.type.name == 'MODIFIED':
            doc = change.document.to_dict()
            if (doc['status'] == 'requestPurchase'):
                startPurchase(doc['amount'])
            elif (doc['status'] == 'requestTopup'):
                startTopup(doc['amount'])

# read uid
def read():
    try:
        while True:
            id = reader.read_id()
            return id
    except:
        print("Error while reading")


def startPurchase(amount):

    fancy_amount = "%.2f" % amount

    lcd.set("Zu zahlen: " + fancy_amount + "EUR",1)
    lcd.set("Chip aufhalten  ",2)

    uid = read()

    lcd.set("Bearbeite...    ",1)
    lcd.set("                ",2)

    buzzer.start(10)
    sleep(1)
    buzzer.stop()

    response = urllib.request.urlopen("https://us-central1-nfc-cash.cloudfunctions.net/processTransaction?tId=1&uid=" + str(uid)).read()
    response = response.decode("utf-8")

    if (response == 'success'):
        lcd.set("Vielen Dank!    ",1)
        lcd.set("                ",2)

    elif (response == 'insufficientCredit'):
        lcd.set("Guthaben nicht  ",1)
        lcd.set("ausreichend"     ,2)
    elif (response == 'unknownCard'):
        lcd.set("Karte nicht     ",1)
        lcd.set("lesbar"          ,2)

    sleep(3)

    lcd.set("Guten Tag!      ",1)
    lcd.set("                ",2)
    return



def startTopup(amount):

    fancy_amount = "%.2f" % amount

    lcd.set("Einzahlung: " + fancy_amount + "EUR",1)
    lcd.set("Chip aufhalten  ",2)

    uid = read()

    lcd.set("Bearbeite...    ",1)
    lcd.set("                ",2)

    buzzer.start(10)
    sleep(1)
    buzzer.stop()

    response = urllib.request.urlopen("https://us-central1-nfc-cash.cloudfunctions.net/processTransaction?tId=1&uid=" + str(uid)).read()
    response = response.decode("utf-8")

    if (response == 'success'):
        lcd.set("Vielen Dank!    ",1)
        lcd.set("                ",2)

    elif (response == 'insufficientCredit'):
        lcd.set("Guthaben nicht  ",1)
        lcd.set("ausreichend",2)

    elif (response == 'unknownCard'):
        lcd.set("Karte nicht     ",1)
        lcd.set("lesbar"          ,2)


    sleep(3)

    lcd.set("Guten Tag!      ",1)
    lcd.set("                ",2 )
    return

# start capturing keyboard intterruptions then initialize GPIO
try :
    # initialize RPi.GPIO
    GPIO.setmode(GPIO.BCM)
    
    # initialize buzzer
    GPIO.setup(23, GPIO.OUT)
    buzzer = GPIO.PWM(23, 523.25)
    GPIO.output(23, True) 

    # initialization completed
    lcd.set("Guten Tag!      ",1)
    lcd.set("                ",2 )

    # attach handler to firebase collection
    query_watch = db.collection(u'bridge').document('process').on_snapshot(handler)
    
    # keep script running
    input()

except KeyboardInterrupt:
    GPIO.cleanup()
